class Code
  attr_reader :pegs

  PEGS = {red:    "R",
          green:  "G",
          blue:   "B",
          yellow: "Y",
          orange: "O",
          purple: "P"}

  def initialize(pegs)
    @pegs = pegs
  end

  def [](idx)
    @pegs[idx]
  end

  def[]=(idx, value)
    @pegs[idx] = value
  end

  def exact_matches(input_code)
    exact_matches = 0
    @pegs.each_index do |idx|
      exact_matches += 1 if input_code[idx] == self[idx]
    end
    exact_matches
  end

  def near_matches(input_code)
    near_matches = []
    input_code.pegs.each do |color|
      near_matches << color if @pegs.include?(color)
    end
    near_matches = near_matches.uniq.length - exact_matches(input_code)
    near_matches <= 0 ? 0 : near_matches
  end


  def ==(input_code)
    return false unless input_code.is_a? Code
    @pegs == input_code.pegs
  end

  def self.parse(string)
    string = string.upcase
    string.chars.each do |color|
      if PEGS.values.include?(color) == false || string.length != 4 || string.nil?
        raise "Invalid Input"
      end
    end
    Code.new(string.split(""))
  end

  def self.random
    code = []
    4.times { code << PEGS.values.sample }
    Code.new(code)
  end

end

class Game

  attr_reader :secret_code, :remaining_guesses

  def initialize(scrt_cd = nil)
    scrt_cd.nil? ? @secret_code = Code.random : @secret_code = scrt_cd
    @remaining_guesses = 10
  end

  def play
    until @remaining_guesses == 0
      print "Please enter your 4-digit code. The colors are: "
      puts "Red, Green, Blue, Yellow, Orange, and Purple. Exp.(BRGB) "
      puts "You have #{remaining_guesses} remaining guesses"
      guess = get_guess
      if guess == @secret_code
        break
      else
        display_matches(guess)
      end
      @remaining_guesses -= 1
    end
    puts "Game Over"
    if guess == @secret_code
      puts "Congratulations! You Won! The winning code was #{@secret_code.pegs}"
    else
      puts "Sorry You Lose... Better Luck Next time. The winning code was #{@secret_code.pegs}"
    end

  end

  def get_guess
    begin
      Code.parse(gets.strip)
    rescue
      puts "Invalid Entry. Please try again"
      retry
    end
  end

  def display_matches(guess)
    puts "You have: \n#{@secret_code.exact_matches(guess).to_s} exact matches"
    puts "#{@secret_code.near_matches(guess).to_s} near matches"
  end

end

if $PROGRAM_NAME == __FILE__
  Game.new.play
end
